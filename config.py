[Basic]
ROOT_DIR = '/home/atm/persona/'
MEDIA_DIR = '/home/atm/persona/content/'
TEMPLATE = '/home/atm/persona/templates/index.html'
SERVE_DIR = '/home/atm/persona/html/'
PUBLIC_URL = '/html/'
STATIC_URL = '../../media/'
AUTHOR = 'Anoop Thomas Mathew'
BASE_URL = 'http://www.infinteloop.in'
#ORDER_BY = 'TIME' #views, 
#BACKGROUND_COLOR = '#123456'



[Repository]
# Repo for the content - any of the version controlled documents
CONTENT_DIR = '/home/atm/persona/content/'
REPO = ''
REMOTE_ACCESS = True

[APIKeys]
# google analytics, etc.

[Advanced]
# To access advanced options and inline test cases
DEBUG = False
# nltk suggestions on.off
# audio reader on.off
# jsonrpc deploy on.off
#disqus on.off -key